﻿using Contestant.Repository.IRepos;
using Contestant.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contestant.Repository;

namespace Contestant.Repository
{
    public class ContestantInfoRepo : CrudRepo<ContestantInfo>,IContestantInfoRepo
    {
        #region Inilization

        private ContestantDBEntities _entities;

        public ContestantInfoRepo(ContestantDBEntities _ent)
            : base(_ent)
        {
            this._entities=_ent;
        }

        #endregion

        #region Method

        public ContestantInfo FindByID(int? ID)
        {
            try
            {
                return _entities.ContestantInfoes.Find(ID);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public List<ContestantInfo> GetActiveContestant()
        {
            var active = (from x in _entities.ContestantInfoes.ToList() where x.IsActive == true select x).ToList();
            return active;
        }

        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_entities != null)
                {
                    _entities.Dispose();
                    _entities = null;
                }
            }
            base.Dispose();
        }

        #endregion
    }
}

﻿using Contestant.Repository.IRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contestant.Entities.Models;
namespace Contestant.Repository
{
    public class DistrictRepo : CrudRepo<District>, IDistriceRepo
    {
         #region Inilization

        private ContestantDBEntities _entities;

        public DistrictRepo(ContestantDBEntities _ent)
            : base(_ent)
        {
            this._entities=_ent;
        }

        #endregion

        #region Method

        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_entities != null)
                {
                    _entities.Dispose();
                    _entities = null;
                }
            }
            base.Dispose();
        }

        #endregion
    }
}

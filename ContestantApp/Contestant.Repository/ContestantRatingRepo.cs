﻿using Contestant.Entities.Models;
using Contestant.Repository.IRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contestant.Repository
{
    public class ContestantRatingRepo : CrudRepo<ContestantRating>, IContestantRatingRepo
    {
        #region Inilization

        private ContestantDBEntities _entities;

        public ContestantRatingRepo(ContestantDBEntities _ent)
            : base(_ent)
        {
            this._entities=_ent;
        }

        #endregion

        #region Method

        public ContestantRating FindByID(int? ID)
        {
            try
            {
                return _entities.ContestantRatings.Find(ID);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public List<ContestantRating> FindByContestantID(int cID)
        {
            var contestant = (from x in _entities.ContestantRatings.ToList() where x.ContestantId == cID select x).ToList();
            return contestant;
        }

        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_entities != null)
                {
                    _entities.Dispose();
                    _entities = null;
                }
            }
            base.Dispose();
        }

        #endregion
    }
}

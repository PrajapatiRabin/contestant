﻿using Contestant.Entities.Models;
using Contestant.Repository.IRepos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contestant.Repository
{
    public abstract class CrudRepo<T> : ICrudRepo<T> where T : class
    {
        #region Inilization

        private ContestantDBEntities _entities;

        private readonly IDbSet<T> _dbset;

        public CrudRepo(ContestantDBEntities entities)
        {
            this._entities = entities;
            this._dbset = _entities.Set<T>();
        }

        #endregion

        #region Virtual Methods

        public virtual List<T> GetAll()
        {
            try
            {
                IQueryable<T> query = _dbset;
                return query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)// this is delegate to which we can passs lamda expression
        {
            try
            {
                IQueryable<T> query = _dbset.Where(predicate);
                return  query.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual T Create(T entity)
        {
            try
            {
                var a = _entities.Set<T>().Add(entity);
                return a;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual T Delete(T entity)
        {
            try
            {
                return _dbset.Remove(entity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void Edit(T entity)
        {
            try
            {
                _entities.Entry(entity).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public virtual int Save()
        {
            try
            {
                int rCount = 0;
                rCount = _entities.SaveChanges();
                return rCount;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public virtual void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_entities != null)
                {
                    _entities.Dispose();
                    _entities = null;
                }
            }
        }

        #endregion

    }
}

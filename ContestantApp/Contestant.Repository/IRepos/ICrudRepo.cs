﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Contestant.Repository.IRepos
{
   public interface ICrudRepo<T> where T:class
    {
        List<T> GetAll();
        List<T> FindBy(Expression<Func<T, bool>> predicate);
        T Create(T entity);
        T Delete(T entity);
        void Edit(T entity);

        int Save();
        void Dispose();
    }
}

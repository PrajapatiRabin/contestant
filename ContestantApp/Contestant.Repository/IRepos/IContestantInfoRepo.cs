﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contestant.Entities.Models;

namespace Contestant.Repository.IRepos
{
    public interface IContestantInfoRepo : ICrudRepo<ContestantInfo>
    {
        ContestantInfo FindByID(int? ID);
        List<ContestantInfo> GetActiveContestant();
    }
}

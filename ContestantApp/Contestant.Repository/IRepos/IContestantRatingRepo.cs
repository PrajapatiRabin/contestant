﻿using Contestant.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contestant.Repository.IRepos
{
    public interface IContestantRatingRepo:ICrudRepo<ContestantRating>
    {
        ContestantRating FindByID(int? ID);
        List<ContestantRating> FindByContestantID(int cID);
        
    }
}

﻿///refrence path='angular.min.js'
///refrence path='module.js'
///refrence path='dashboardService.js' //service

'use strict'
contestantController.controller('rate', ['$scope', 'cService','cId', '$modalInstance', function ($scope, cService,cId, $modalInstance) {

    $scope.ratingVal = {
        value:1
    };
    
    $scope.rate = function () {
        var contestantRating = {
            ContestantId:cId ,
            Rating:$scope.ratingVal.value
        }

        var result = cService.rateContestant(contestantRating);
        result.then(function (response) {
            $scope.close();
        })

    }
    $scope.close = function () {
        $modalInstance.dismiss('cancel');
    };
}])

contestantController.controller('contestant', ['$scope', 'cService', '$modal', function ($scope, cService, $modal) {
    //()
    onLoad();
   
    function onLoad() {
            
        getContestant(null,null);
        //paging();
    }

    function getContestant(from,to)
    {
        var result = cService.getContestantRating(from, to);

        result.then(function (response) {
            
            $scope.data = JSON.parse(response);
            $scope.cId = $scope.data[0].ContestantId;
        }, function (error) { console.log(error) });

    }

    $scope.searchContestant = function () {
        console.log($scope.dt)
        console.log($scope.to)
        if ($scope.dt == "undefined" || $scope.to == "undefined") {
            getContestant(null, null)
        }
        else {
            var _mm = $scope.dt.getMonth();
            var _dd = $scope.dt.getDate();
            var _yyyy = $scope.dt.getFullYear();

            var fromdate = _mm + '/' + _dd + '/' + _yyyy;

            var _mm = $scope.to.getMonth();
            var _dd = $scope.to.getDate();
            var _yyyy = $scope.to.getFullYear();

            var todate = _mm + '/' + _dd + '/' + _yyyy;
            getContestant(fromdate, todate);
        }
    }

    //modal for ingredients
    $scope.rate = function (item) {
        $scope.cId = item.ContestantId;
        var modalInstance = $modal.open({
            templateUrl: 'rating.html',
            controller: 'rate',
            resolve: {
                cId: function () {
                    return $scope.cId;
                }
            }
        });

        modalInstance.result.then(function () {
            alert("hey")
           // $scope.searchContestant();
        }, function () {
            getContestant(null, null)
        });
    }

    $scope.today = function () {
        $scope.dt = new Date();
    };
    //$scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function (date, mode) {
        return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };

    $scope.toggleMin = function () {
        $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.maxDate = new Date(2020, 5, 22);

    $scope.open = function ($event) {
        $scope.status.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];

    $scope.status = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 2);
    $scope.events =
      [
        {
            date: tomorrow,
            status: 'full'
        },
        {
            date: afterTomorrow,
            status: 'partially'
        }
      ];

    $scope.getDayClass = function (date, mode) {
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    };
    /****************************************pagination***************************************/
    function paging() {
        //paging
        $scope.itemsPerPage = 5;
        $scope.currentPage = 0;
        $scope.items = [];//data list
        $scope.range = function () {
            var rangeSize = 5;
            var ps = [];
            var start;

            start = $scope.currentPage;
            if (start > $scope.pageCount() - rangeSize) {
                start = $scope.pageCount() - rangeSize + 1;
            }

            for (var i = start; i < start + rangeSize; i++) {
                if (i >= 0)
                    ps.push(i);
            }
            return ps;
        };

        $scope.prevPage = function () {
            if ($scope.currentPage > 0) {
                $scope.currentPage--;
            }
        };

        $scope.DisablePrevPage = function () {
            return $scope.currentPage === 0 ? "disabled" : "";
        };

        $scope.pageCount = function () {
            return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
        };

        $scope.nextPage = function () {
            if ($scope.currentPage < $scope.pageCount()) {
                $scope.currentPage++;
            }
        };

        $scope.DisableNextPage = function () {
            return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
        };

        $scope.setPage = function (n) {
            $scope.currentPage = n;
        };
    }

}]);

contestantController.controller('gallery', ['$scope', 'cService','Lightbox', function ($scope, cService,Lightbox) {
    
    $scope.images = [];
    gallery()
    function gallery() {
        
        var result = cService.getGalleryData();
        result.then(function (response) {
            var data = JSON.parse(response.data)
            for (var item in data)
            {
                $scope.images.push({ url: data[item].PhotoUrl })
            }
        })
    }


    $scope.openLightboxModal = function (index) {
        console.log($scope.images)
        Lightbox.openModal($scope.images,index);
    };
}])
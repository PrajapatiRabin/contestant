﻿///<reference path="angular.min.js" />

var app = angular.module("Contestant", ['contestantController', 'contestantServices', 'pagination', 'ui.bootstrap', 'bootstrapLightbox']);

var contestantController = angular.module("contestantController", []);

var contestantServices = angular.module("contestantServices", []);

var pagingFilter = angular.module("pagination", []);

var contestantDirective = angular.module('contestantDirective', []);
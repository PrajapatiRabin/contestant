﻿///refrence path='angular.min.js'
///refrence path='module.js'

'use strict'

contestantServices.factory('cService', ['$http', '$q', function ($http, $q) {

    /*    Start:Non-Finalcial Transaction  */
    function GetDistrict()
    {
        var defered = $q.defer();
        var request = $http({
            method: "get",
            url: "/ContestantInfoes/GetDistrict"
        });
        request.then(function (response) {
            defered.resolve(response.data)
        }, function (error) {
            defered.reject(error);
        });
        return defered.promise;
    }

    function Map(id)
    {
        var defered = $q.defer();
        $http({
                 method: 'get',
                 url: '/Map/GetContestantFromDistrict?_dID='+id
             }).then(function (response) {
                 defered.resolve(response)
             }, function (error) {
                 defered.reject(error)
                 //throw error.status;
             });
        return defered.promise;
    }

    function rateContestant(_data) {
        var deferred = $q.defer();
        var request = $http(
            {
                method: 'post',
                url: '/ContestantRating/RateContestant',
                data: _data,
                /*headers: {
                    'RequestVerificationToken': _token
                }*/
            });

        request.then(function (response) {
            deferred.resolve(response)
        }, function (error) {
            deferred.reject(error);
        })

        return deferred.promise;
    }

    function getContestantRating(from,to) {
        var defered = $q.defer();
        var request = $http({
            method: "get",
            url: "/ContestantRating/GetActiveContestantRating?_from=" + from + "&to=" + to
        });
        request.then(function (response) {
            console.log(response);
            defered.resolve(response.data)
        }, function (error) {
            defered.reject(error);
        });
        return defered.promise;
    }

   
    function CreateContestant(_data) {
        var defered = $q.defer();
        $http(
             {
                 method: 'post',
                 url: '/ContestantInfoes/Create',
                 data: _data,
                 headers: { 'Content-Type': undefined }
             }).then(function (response) {
                 defered.resolve(response.status)
             }, function (error) {
                 defered.reject(error)
                 //throw error.status;
             });
        return defered.promise;
    }


    function uplodafile(_data) {
        console.log(_data)
        var defered = $q.defer();
        $http(
             {
                 method: 'post',
                 url: '/ContestantInfoes/FileUpload',
                 data: _data,
                 headers: { 'Content-Type': undefined }
             }).then(function (response) {
                 defered.resolve(response.status)
             }, function (error) {
                 defered.reject(error)
                 //throw error.status;
             });
        return defered.promise;
    }

    function getGalleryData() {
        var defered = $q.defer();
        $http(
             {
                 method: 'get',
                 url: '/Gallery/GetGallareyData'
             }).then(function (response) {
                 defered.resolve(response)
             }, function (error) {
                 defered.reject(error)
                 //throw error.status;
             });
        return defered.promise;
    }

    

    var serviceFactory = {
        GetDistrict: GetDistrict,
        CreateContestant: CreateContestant,
        uplodafile: uplodafile,
        getContestantRating: getContestantRating,
        rateContestant: rateContestant,
        getGalleryData: getGalleryData,
        Map: Map
    }

    return serviceFactory;
}]);


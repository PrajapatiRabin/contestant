﻿using Contestant.Entities.Models;
using Contestant.Repository.IRepos;
using ContestantApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ContestantApp.Controllers
{
    public class ContestantRatingController : Controller
    {
        private IContestantRatingRepo _crate;
        private IContestantInfoRepo _cinfo;

        public ContestantRatingController(IContestantRatingRepo _crate, IContestantInfoRepo _cinfo)
        {
            this._crate = _crate;
            this._cinfo = _cinfo;
        }

        // GET: ContestantRating

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetActiveContestantRating(DateTime? _from,DateTime? to)
        {
            try
            {
                var _crVMList = this.GetContestRatingVMList(_from, to);
                if (_crVMList.Count() > 0)
                {
                    var jdata = JsonConvert.SerializeObject(_crVMList);
                    return Json(jdata, JsonRequestBehavior.AllowGet);
                }
                return Json( JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private List<ContestantRatingViewModel> GetContestRatingVMList(DateTime? _from, DateTime? to)
        {
            try
            {
                List<ContestantRatingViewModel> crVMList = new List<ContestantRatingViewModel>();
                var activeContestant = _cinfo.GetActiveContestant();
                var contestantRated = _crate.GetAll().ToList();
                if (contestantRated.Count() > 0)
                {
                    if (activeContestant.Count() > 0)
                    {
                        DateTime? min = null;
                        DateTime? max = null;
                        if (_from == null || to == null)
                        {
                            max = contestantRated.Max(x => x.RatedDate);
                            min = contestantRated.Min(x => x.RatedDate);
                        }
                        else
                        {
                            if (_from > to)
                            {
                                max = _from;
                                min = to;
                            }
                            else
                            {
                                max = to;
                                min = _from;
                            }
                        }
                        var _countBetweenDate = (from x in contestantRated where x.RatedDate >= min where x.RatedDate <= max select x).ToList();

                        var _dividBy = _countBetweenDate.Count();
                        if (_dividBy == 0)
                        {
                            _dividBy = 1;
                        }

                        foreach (var item in activeContestant)
                        {
                            var contestantId = item.Id;
                            var ratedContestant = _crate.FindByContestantID(contestantId);
                            var sumRate = ratedContestant.Sum(x => x.Rating);

                            var a = Convert.ToDecimal(sumRate) / _dividBy;
                            var avgRating =Math.Round(a, 2) ;

                            ContestantRatingViewModel crvM = new ContestantRatingViewModel()
                            {
                                ContestantId = item.Id,
                                AverageRating = avgRating,
                                FirstName = item.FirstName,
                                LastName = item.LastName,
                                District = item.District.Name,
                                DOB = item.DateOfBirth.ToString()
                            };
                            crVMList.Add(crvM);
                        }

                        return crVMList;
                    }
                    return crVMList;
                }
                else
                {
                    foreach (var item in activeContestant)
                    {
                        var contestantId = item.Id;
                        var avgRating = 0;

                        ContestantRatingViewModel crvM = new ContestantRatingViewModel()
                        {
                            ContestantId = item.Id,
                            AverageRating = avgRating,
                            FirstName = item.FirstName,
                            LastName = item.LastName,
                            District = item.District.Name,
                            DOB = item.DateOfBirth.ToString()
                        };
                        crVMList.Add(crvM);
                    }
                    return crVMList;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult RateContestant(ContestantRating cr)
        {
            if(cr!=null)
            {
                cr.RatedDate = DateTime.Now;
                _crate.Create(cr);
                _crate.Save();

                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _crate.Dispose();
                _cinfo.Dispose();
            }
            base.Dispose(disposing);
        }
       
    }
}
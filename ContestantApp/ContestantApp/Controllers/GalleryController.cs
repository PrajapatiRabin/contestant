﻿using Contestant.Entities.Models;
using Contestant.Repository.IRepos;
using ContestantApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContestantApp.Controllers
{
    public class GalleryController : Controller
    {

        private IContestantInfoRepo _cinfo;
        
        // GET: Gallery

        public GalleryController(IContestantInfoRepo _cinfo)
        {
            this._cinfo=_cinfo;
        }
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetGallareyData()
        {
            List<GallaeryViewMode> gvmL = new List<GallaeryViewMode>();
            var contestant = _cinfo.GetAll().ToList();
            var active = (from x in contestant where x.IsActive == true select x).ToList();
            foreach(var item in active)
            {
                GallaeryViewMode gvm = new GallaeryViewMode();
                gvm.ContestantID = item.Id;
                gvm.PhotoUrl = item.PhotoUrl;
                gvmL.Add(gvm);
            }
            
            
            var jdata = JsonConvert.SerializeObject(gvmL);
            return Json(jdata, JsonRequestBehavior.AllowGet);
        }
    }
}
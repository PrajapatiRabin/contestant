﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Contestant.Entities.Models;
using ContestantApp.Models;
using System.Globalization;
using Newtonsoft.Json;
using PagedList;
using Contestant.Repository.IRepos;

namespace ContestantApp.Controllers
{
    public class ContestantInfoesController : Controller
    {
        private IContestantInfoRepo _cinfo;
        private IDistriceRepo _dist;

        public ContestantInfoesController(IContestantInfoRepo cr, IDistriceRepo _dist)
        {
            this._cinfo = cr;
            this._dist = _dist;
        }

        // GET: ContestantInfoes
        public ActionResult Index(int? page)
        {
            var contestantInfoes = _cinfo.GetAll().ToList();

            //paging
            var pageNumber = page ?? 1;
            var itemPerPage = contestantInfoes.ToPagedList(pageNumber,5);
            return View(itemPerPage);
        }
        

        // GET: ContestantInfoes/Create
        public ActionResult Create()
        {
            ContestantCreateViewModel crm = new ContestantCreateViewModel();
            crm.Gender = false;
            ViewBag.DistrictId = new SelectList(_dist.GetAll().ToList(), "Id", "Name");
            return View();
        }

        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ContestantCreateViewModel contestantInfo)
        {
            if (ModelState.IsValid)
            {
                var file = contestantInfo.Image;
                string _imageUID = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                string fileLocation="";
                if (file != null && file.ContentLength > 0)
                {
                    var filename = file.FileName;
                    var _newfilename = _imageUID;
                    fileLocation = string.Format("{0}/{1}{2}", "/images", _newfilename, System.IO.Path.GetExtension(filename));
                    string saveat = string.Format("{0}/{1}{2}", Server.MapPath("~/images"), _newfilename, System.IO.Path.GetExtension(filename));
                    file.SaveAs(saveat);
                }

                var dob = DateTime.ParseExact(contestantInfo.Dob, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                var contestant = new ContestantInfo()
                {
                    FirstName=contestantInfo.FirstName,
                    LastName=contestantInfo.LastName,
                    DateOfBirth =dob,
                    IsActive=contestantInfo.IsActive,
                    DistrictId=contestantInfo.DistrictId,
                    PhotoUrl=fileLocation,
                    Address=contestantInfo.Address
                };

                if(contestantInfo.Gender==false||contestantInfo.Gender==null)
                {
                    contestant.Gender = "female";
                }
                else
                {
                    contestant.Gender = "male";
                }

                _cinfo.Create(contestant);
                _cinfo.Save();
                return RedirectToAction("Index");
            }

            ViewBag.DistrictId = new SelectList(_dist.GetAll().ToList(), "Id", "Name", contestantInfo.DistrictId);
            return View(contestantInfo);
            
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContestantInfo contestantInfo = _cinfo.FindByID(id);
            /*var date = Convert.ToString(contestantInfo.DateOfBirth);
            var dob = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);*/
            var contestantvm = new ContestantEditViewModel()
            {
                ContestantID = contestantInfo.Id,
                FirstName = contestantInfo.FirstName,
                LastName = contestantInfo.LastName,
                Dob = contestantInfo.DateOfBirth.ToString(),
                IsActive = contestantInfo.IsActive,
                DistrictId = Convert.ToInt32(contestantInfo.DistrictId),
                ImageUrl = contestantInfo.PhotoUrl,
                Address = contestantInfo.Address
            };

            if (contestantInfo.Gender == "female")
            {
                contestantvm.Gender = false;
            }
            else
            {
                contestantvm.Gender = true;
            }
            if (contestantvm == null)
            {
                return HttpNotFound();
            }
            ViewBag.DistrictId = new SelectList(_dist.GetAll().ToList(), "Id", "Name", contestantInfo.DistrictId);
            return View(contestantvm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ContestantEditViewModel contestantvm)
        {
            //to Edit Image or photo
            //create separate page
            // like edit photo
            // but not now
            if (ModelState.IsValid)
            {
                var contestant = _cinfo.FindByID(contestantvm.ContestantID);
                bool chk = this.chkIfDateParsabel(contestantvm.Dob);
                DateTime? dob=null;
               if(chk==true)
               {
                   dob = DateTime.ParseExact(contestantvm.Dob, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
               }
               else
               {
                   dob=Convert.ToDateTime(contestantvm.Dob);
               }
               
                contestant.FirstName = contestantvm.FirstName;
                contestant.LastName = contestantvm.LastName;
                contestant.DateOfBirth = dob;
                contestant.IsActive = contestantvm.IsActive;
                contestant.DistrictId = contestantvm.DistrictId;
                contestant.PhotoUrl = contestantvm.ImageUrl;
                contestant.Address = contestantvm.Address;

                if (contestantvm.Gender == false)
                {
                    contestant.Gender = "female";
                }
                else
                {
                    contestant.Gender = "male";
                }


                _cinfo.Edit(contestant);
                _cinfo.Save();
                return RedirectToAction("Index");
            }
            ViewBag.DistrictId = new SelectList(_dist.GetAll().ToList(), "Id", "Name", contestantvm.DistrictId);
            return View(contestantvm);
        }

        private bool chkIfDateParsabel(string d)
        {
            bool _ok = false;
            try
            {
                var dob = DateTime.ParseExact(d, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                _ok = true;
                return _ok;
            }
            catch(Exception ex)
            {
                return _ok;
            }
        }

        // GET: ContestantInfoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ContestantInfo contestantInfo = _cinfo.FindByID(id);
            if (contestantInfo == null)
            {
                return HttpNotFound();
            }
            return View(contestantInfo);
        }

        // POST: ContestantInfoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ContestantInfo contestantInfo = _cinfo.FindByID(id);
            _cinfo.Delete(contestantInfo);
            _cinfo.Save();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _cinfo.Dispose();
                _dist.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

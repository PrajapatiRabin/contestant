﻿using Contestant.Repository.IRepos;
using ContestantApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ContestantApp.Controllers
{
    public class MapController : Controller
    {
        // GET: Map
        private IContestantInfoRepo _cinfo;
        //private IDistriceRepo _dist;

        public MapController(IContestantInfoRepo _cinfo)
        {
            this._cinfo = _cinfo;
        }

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetContestantFromDistrict(int _dID)
        {
            var allContestant = _cinfo.GetAll().ToList();
            var _districtContestant = (from x in allContestant where x.DistrictId == _dID select x).ToList();

            List<MapViewModel> mvmL = new List<MapViewModel>();
            foreach(var item in _districtContestant)
            {
                MapViewModel mvm = new MapViewModel()
                {
                    FirstName=item.FirstName,
                    LastName=item.LastName,
                    Dob=item.DateOfBirth.ToString(),
                    Gender=item.Gender
                };

                mvmL.Add(mvm);
            }
            var jdata = JsonConvert.SerializeObject(mvmL);
            return Json(jdata,JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _cinfo.Dispose();
                // _dist.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
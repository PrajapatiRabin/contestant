using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using Contestant.Entities.Models;
using Contestant.Repository.IRepos;
using Contestant.Repository;

namespace ContestantApp
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<ContestantDBEntities>();
            container.RegisterType<IContestantInfoRepo,ContestantInfoRepo>();
            container.RegisterType<IDistriceRepo, DistrictRepo>();
            container.RegisterType<IContestantRatingRepo,ContestantRatingRepo>();
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}
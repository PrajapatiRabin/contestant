﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ContestantApp.Models
{
    public class ContestantCreateViewModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Dob { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public int DistrictId { get; set; }

        [Required]
        public bool Gender { get; set; }

        [Required]
        public string Address { get; set; }

         [Required]
        public HttpPostedFileBase Image { get; set; }

        public string ImageUrl { get; set; }
    }

    public class ContestantEditViewModel
    {
        
        public int ContestantID { get; set; }
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Dob { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public int DistrictId { get; set; }

        [Required]
        public bool Gender { get; set; }

        [Required]
        public string Address { get; set; }

        public HttpPostedFileBase Image { get; set; }

        public string ImageUrl { get; set; }
    }

    public class ContestantRatingViewModel
    {
        public int ContestantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string District { get; set; }
        public decimal AverageRating { get; set; }
    }

    public class GallaeryViewMode
    {
        public int ContestantID { get; set; }
        public string PhotoUrl { get; set; }
    }

    public class MapViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Dob { get; set; }
        public string Gender { get; set; }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ContestantApp.Startup))]
namespace ContestantApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

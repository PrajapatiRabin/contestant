//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Contestant.Entities.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    public partial class ContestantRating
    {
        [Key]
        public int id { get; set; }
        [Required]
        public int ContestantId { get; set; }
        [Required]
        public int Rating { get; set; }
        public System.DateTime RatedDate { get; set; }
    
        public virtual ContestantInfo ContestantInfo { get; set; }
    }
}

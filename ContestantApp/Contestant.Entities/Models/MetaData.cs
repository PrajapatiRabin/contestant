﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contestant.Entities.Models
{
    public class ContestantMetadata
    {
        [Key]
        [Required]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }
        
        [Required]
        public string LastName { get; set; }

        [Required]
        public Nullable<System.DateTime> DateOfBirth { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public Nullable<int> DistrictId { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        public string PhotoUrl { get; set; }
        
        [Required]
        public string Address { get; set; }
    }
}
